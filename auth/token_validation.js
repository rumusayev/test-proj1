const { checkToken } = require("../api/users/user.service");

module.exports = {
  checkAuth: (req, res, next) => {
    if (!req.query.access_token) {
      return res.send({
        success: 0,
        message: "Token was not found"
      });
    }
    checkToken(req.query.access_token, (err, results) => {

      if (err) {
        console.log(err);
      }

      if (!results.cnt) {
        return res.send({
          success: 0,
          message: "User should be authenticated"
        });
      }
      if (results.cnt) {
        req.user_id = results.user_id;
        next();
      }
    });
  }
};
