-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: localhost    Database: Test_proj
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tokens`
--

DROP TABLE IF EXISTS `tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_token` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tokens`
--

LOCK TABLES `tokens` WRITE;
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
INSERT INTO `tokens` VALUES (1,'b06muztvvbphz5egfqmdru9biqoy1656u9dp76me9f7yuh2oqhe1k44f2l3k95rzqi357ia4dwgb8a981u4sv3vb8g76z71t13ms',3,0),(2,'zij308enetgexjdz9bhx0b9tsehjjqwhxhxb8pwmrzbnip98d93wes3zjaukxn1amunhnkysa3xw4v9vg0cwb9ztttfv9uvpflbe',3,0),(3,'9we0ejro1rhzs1yw1b9m6m3wbeezetsfr0o1fehof968jaudqtkarc61up9zr2p4i2zsy23qht1h9xi12yv0fdy52x75xn1dv7h6',3,0),(4,'lwhonp83i93722qj71pew2twwv6rt13iik9kunma6tq0qlqbt9foc8d2wpply8abwdadvveed3qygjgfo6sdp7indkuwz0xl4693',3,0),(5,'4hgd5nleby5w807wy75k5evg7s7zer06iiu1b35o8ofx91z4j9hdymu3lor2q07nmfparanoxa42fojym4lpahoyoay5qih2jwgg',3,0),(6,'fmkgohofj7dh5u685dyebvuubrh69k8h1rkf9gel3uxyt09839q2pnrw3qecyc55yz9sns78c608kiikjh28ya7marx5auxwr570',3,0),(7,'eqwcw31mwl437ss1o96c6dgkugt06929u3gkbkdps98x8vblmxpe006jrvzv4jqhs2je3kjzqffe2s43d0vsar94s0hk8ic4gfj9',3,0),(8,'w9etkxzgzpfou68cxdnx8py0s1wo687zxb4er9ldd4uhtld5crhufgqamrzy77v46a2d4dt37fen51q27h9pckdad4sctftb7zc0',3,0),(9,'a9372drdm2igotg8oqicj42sokbyqepyvblc986njvqhwa2i6emqiqp63do1x7mdrxoxe0ot353skkjtiz4e8mkdff23upp0ef70',3,1);
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_data`
--

DROP TABLE IF EXISTS `user_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `data` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_data`
--

LOCK TABLES `user_data` WRITE;
/*!40000 ALTER TABLE `user_data` DISABLE KEYS */;
INSERT INTO `user_data` VALUES (4,3,'{\"b\":\"test\",\"test\":\"backend\"}'),(5,2,'{\"b\":\"test\",\"test\":\"backend\"}'),(6,3,'{\"b\":\"test\",\"test\":\"backend\"}');
/*!40000 ALTER TABLE `user_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(45) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'Ruslan Kuznecov','test@gmail.com','e10adc3949ba59abbe56e057f20f883e'),(3,'Ruslan Musayev','musayev.ruslan88@gmail.com','c33367701511b4f6020ec61ded352059');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-17 10:13:31
