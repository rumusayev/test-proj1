const pool = require("../../config/database");

module.exports = {
    insertUserData: (userId, data, callBack) => {
        pool.query(
            'INSERT INTO user_data (user_id, `data`) VALUES (?, ?)',
            [userId, data],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results);
            }
        );
    },

    getAllUserData: (userId, callBack) => {
        pool.query(
            'SELECT id, `data` FROM user_data WHERE user_id=?',
            [userId],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results);
            }
        );
    },

    getUserDataById: (dataId, userId, callBack) => {
        pool.query(
            'SELECT id, `data` FROM user_data WHERE user_id=? AND id=?',
            [userId, dataId],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results[0]);
            }
        );
    },
};
