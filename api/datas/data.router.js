const router = require("express").Router();
const { checkAuth } = require("../../auth/token_validation");
const {
    save,
    getAll,
    getOne
} = require("./data.controller");



router.post("/save", checkAuth, save);
router.get("/list", checkAuth, getAll);
router.get("/get/:id", checkAuth, getOne);

module.exports = router;
