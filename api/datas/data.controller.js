const {
    insertUserData,
    getAllUserData,
    getUserDataById
} = require("./data.service");

module.exports = {
    save: (req, res) => {
        const body = req.body;

        insertUserData(req.user_id, JSON.stringify(req.body), (err, results) => {
            if (err) {
                console.log(err);
            }

            return res.json({
                data: !!results
            });
        });
    },

    getAll: (req, res) => {
        getAllUserData(req.user_id, (err, results) => {
            let parsedData = [];
            if (err) {
                console.log(err);
            }

            results.forEach(element => {
                // console.log(element.data)
                parsedData.push(
                    {
                        id: element.id,
                        data: JSON.parse(element.data)
                    }
                );
            });

            if (results) {
                return res.json({
                    data: parsedData
                });
            }

            return res.json({
                success: 1,
                message: "No data found"
            });
        });
    },

    getOne: (req, res) => {
        getUserDataById(req.params.id, req.user_id, (err, result) => {
            if (err) {
                console.log(err);
            }

            if (!result) {
                return res.json({
                    success: 0,
                    message: "No data with such id found"
                });
            }

            if (result) {
                return res.json({
                    success: 1,
                    data: {
                        id: result.id,
                        data: JSON.parse(result.data)
                    }
                });
            }
        });
    }
};