const {
  getUserByUserEmail,
  checkExistingToken,
  createUserToken,
  deactivateToken,
  checkToken
} = require("./user.service");
const md5 = require('md5');

module.exports = {
  login: (req, res) => {
    const body = req.body;
    getUserByUserEmail(body.email, md5(body.password), (err, results) => {
      if (err) {
        console.log(err);
      }
      if (!results) {
        return res.json({
          success: 0,
          data: "Invalid email or password"
        });
      }

      if (results) {
        checkExistingToken(results.id, (err, chTokenResults) => {

          if (err) {
            console.log(err);
          }

          if (!chTokenResults) {
            let access_token = [...Array(100)].map(i=>(~~(Math.random()*36)).toString(36)).join('');
            createUserToken(results.id, access_token, (err, crTokenResult) => {
              if (err) {
                console.log(err);
              }

              if (crTokenResult) {
                return res.json({
                  user_id: results.id,
                  user: body.email,
                  access_token: access_token
                });

              }
            });
          }

          if (chTokenResults) {
            return res.json({
              user_id: results.id,
              user: body.email,
              access_token: chTokenResults.access_token
            });
          }

        });
      }
    });
  },
  logout: (req, res) => {
    const body = req.body;
    deactivateToken(body.user_id, body.access_token, (err, results) => {
      if (err) {
        console.log(err);
      }

      if (results) {
        return res.json({
          success: !!results.affectedRows
        });
      }


      return res.json({
        success: false
      });
    });
  },
  checkUserToken: (req, res) => {
    const body = req.body;

    checkToken(req.query.access_token, (err, chTokenResults) => {

      if (err) {
        console.log(err);
      }

      return true;
    });
  }
};
