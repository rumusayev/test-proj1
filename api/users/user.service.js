const pool = require("../../config/database");

module.exports = {
  getUserByUserEmail: (email, password, callBack) => {
    pool.query(
      `select * from users where email = ? AND password = ?`,
      [email, password],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results[0]);
      }
    );
  },
    checkExistingToken: (userId, callBack) => {
      pool.query(
          `select * from tokens where user_id = ? AND is_active = 1 LIMIT 1`,
          [userId],
          (error, results, fields) => {
              if (error) {
                  callBack(error);
              }
              return callBack(null, results[0]);
          }
      )
    },
    createUserToken: (userId, token, callBack) => {
        pool.query(
            "INSERT INTO tokens (access_token, user_id) VALUES (?, ?)",
            [token, userId],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results);
            }
        )
    },
    deactivateToken: (userId, token, callBack) => {
      pool.query(
          "UPDATE tokens SET is_active=0 WHERE user_id=? AND access_token=? AND is_active=1",
          [userId, token],
          (error, results, fields) => {
              if (error) {
                  callBack(error);
              }
              return callBack(null, results);
          }
      )
    },
    checkToken: (token, callBack) => {
        pool.query(
            `select user_id, count(*) cnt from tokens where access_token = ? AND is_active = 1 group by user_id`,
            [token],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results[0]);
            }
        )
    },
};
