require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const userRouter = require("./api/users/user.router");
const dataRouter = require("./api/datas/data.router");


app.use(bodyParser.json({limit: '50mb', extended: true}))
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}))

app.use(express.json());

app.use("/api/users", userRouter);
app.use("/api/datas", dataRouter);

const port = process.env.PORT || 4000;
app.listen(port, () => {
  console.log("server up and running on PORT :", port);
});
